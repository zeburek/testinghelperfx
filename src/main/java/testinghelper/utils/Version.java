package testinghelper.utils;

import testinghelper.Testinghelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Properties;

/**
 * Created by zeburek on 03.07.2017.
 */
public class Version {
    public volatile static String VERSION;
    public Version(){
        VERSION = getVersionFromFile();
    }

    public String getVersionFromFile() {
        try (InputStream stream = Testinghelper.class.getResourceAsStream("version.properties")) {
            Properties verProp = new Properties();
            verProp.load(stream);
            String major = verProp.getProperty("VERSION_MAJOR");
            String minor = verProp.getProperty("VERSION_MINOR");
            String subminor = verProp.getProperty("VERSION_SUBMINOR");
            String code = verProp.getProperty("VERSION_CODE");
            return major+"."+minor+"."+subminor+"-"+code;
        } catch (IOException e) {
            e.printStackTrace();
            return "1.0";
        }
    }
}
