/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testinghelper.controllers;

import com.brsanthu.googleanalytics.AppViewHit;
import com.brsanthu.googleanalytics.EventHit;
import com.brsanthu.googleanalytics.GoogleAnalytics;
import testinghelper.utils.LoggingAdapter;

import static testinghelper.Testinghelper.ENABLE_DEBUG;
import static testinghelper.Testinghelper.VER_ID;

/**
 *
 * @author zeburek
 */
public class AnalyticsController {
    private final static GoogleAnalytics G_A = new GoogleAnalytics("UA-93471795-1","Zero Helper",VER_ID);
    
    
    public AnalyticsController(){
        G_A.postAsync(new AppViewHit("Zero Helper", VER_ID, "Started"));
        G_A.getConfig().setGatherStats(true);
        LoggingAdapter.stat("Analytics","Initiated","Stats: "+G_A.getConfig().isGatherStats());
    }
    
    public void trackStatistic(String trackedMessage){
        LoggingAdapter.stat(trackedMessage,trackedMessage);
        if (ENABLE_DEBUG) return;
        G_A.postAsync(new EventHit(trackedMessage, trackedMessage));
    }
    
    public void trackStatistic(String trackedName, String trackedMessage){
        trackStatistic(trackedName, trackedMessage,trackedName);
    }

    public void trackStatistic(String trackedName, String trackedMessage, String trackedValue){
        LoggingAdapter.stat(trackedName,trackedMessage,trackedValue);
        if (ENABLE_DEBUG) return;
        G_A.postAsync(new EventHit(trackedName,trackedMessage,trackedValue,1));
    }
}
